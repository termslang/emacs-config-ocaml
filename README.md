# My EMACS configuration

# Installation
Install EMACS. Don't run it. Clone the repo:

```
git clone https://github.com/termslang/emacs-haskell-config ~/.emacs.d
```

Run EMACS


# OCAML SETUP
```

brew install opam
opam init
eval $(opam env)
brew install ocaml gmp
opam install merlin ocp-indent tuareg utop
 
brew install pkg-config

brew install gmp
cd /usr/local
sudo mkdir include
sudo chown -R $(whoami) $(brew --prefix)/include
brew link gmp

opam install conf-gmp-powm-sec
opam install cryptokit zarith core

opam user-setup install
```
